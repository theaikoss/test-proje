<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Product;
use App\Category;

class ProductController extends Controller
{
    public function index(){
        $products = Product::all();
        $categories = Category::withCount('products')->get();
        return view('frontend.product.index', ['products' =>$products, 'categories' => $categories]);
    }

    public function byCategory($id){
        $products = Product::where('category_id', '=', $id)->get();
        // dd($products);
        $categories = Category::withCount('products')->get();
        return view('frontend.product.index', ['products' => $products, 'categories' => $categories]);
    }

    public function show($id){
        $product = Product::with('category')->get()->find($id);
        // dd($product);
        $categories = Category::withCount('products')->get();
        return view('frontend.product.show', ['product'=>$product, 'categories' => $categories]);
    }
}

<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Product;
use App\Category;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        return view('backend.product.index', ['products' => $products]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('backend.product.create', ['categories' => $categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->has('product_thumbnail')){
            $thumbnail_uploaded = $request->file('product_thumbnail');
            $thumbnail_name = time().".". $thumbnail_uploaded->getClientOriginalExtension();
            $thumbnail_path = public_path('/images');
            $thumbnail_uploaded->move($thumbnail_path, $thumbnail_name);
            $product = Product::create([
                'product_name'      => $request['product_name'],
                'product_content'   => $request['product_content'],
                'product_thumbnail' => '/images/'. $thumbnail_name,
                'category_id'       => $request['category_id'],
                'product_stock'       => $request['product_stock'],
            ]);
        }else{
            $product = Product::create([
                'product_name'      => $request['product_name'],
                'product_content'   => $request['product_content'],
                'category_id'       => $request['category_id'],
                'product_stock'       => $request['product_stock'],
            ]);
        }

        // $thumbnail = $request->file('product_thumbnail')->store('images');
        // $imageName = time().'.'.$request->file('product_thumbnail')->getClientOriginalExtension();  
        // $request->product_image->move(public_path('images'), $imageName);
        // // dd($request);
        // $product = Product::create($request->only(['product_name', 'product_content', 'product_thumbnail', 'category_id', 'product_stock']));

        return redirect()->route('product.edit', $product);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        echo "l";
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $product = Product::find($product);
        $categories = Category::all();
        // dd($product);
        return view('backend.product.edit', ['product' => $product, 'categories' => $categories]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        if($request->has('product_thumbnail')){
            $thumbnail_uploaded = $request->file('product_thumbnail');
            $thumbnail_name = time().".". $thumbnail_uploaded->getClientOriginalExtension();
            $thumbnail_path = public_path('/images');
            $thumbnail_uploaded->move($thumbnail_path, $thumbnail_name);
            $product->update([
                'product_name'      => $request['product_name'],
                'product_content'   => $request['product_content'],
                'product_thumbnail' => '/images/'. $thumbnail_name,
                'category_id'       => $request['category_id'],
                'product_stock'       => $request['product_stock'],
            ]);
        }else{
            $product->update([
                'product_name'      => $request['product_name'],
                'product_content'   => $request['product_content'],
                'category_id'       => $request['category_id'],
                'product_stock'     => $request['product_stock'],
            ]);
        }

        return redirect()->route('product.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();
        return redirect()->route('product.index');
    }
}

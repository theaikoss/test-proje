<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Product;

class DashboardController extends Controller
{
    public function index(){
        $products = Product::all();
        return view('backend.dashboard', ['products' => $products]);
    }
}

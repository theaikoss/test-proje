<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Product;
use App\Category;

class ProductController extends Controller
{
    public function index(){
        $products = Product::all();
        return response()->json(['products' => $products], 200);
    }

    public function show($id){
        $product = Product::with('category')->get()->find($id);
        return response()->json(['product' => $product], 200);
    }
}

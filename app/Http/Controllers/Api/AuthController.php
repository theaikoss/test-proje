<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;

class AuthController extends Controller
{
    public $successResponse = 200;
    

    public function register(Request $request){

        // $validate = Validator::make($request->all(),[
        //     'name' => 'required',
        //     'email' => 'required|email|unique:users',
        //     'password' => 'required',
        //     'c_password' => 'required|same|password'
        // ]);

         $input = $request->all();
         $input['pasword'] = bcrypt($input['password']);
         $user= User::create($input);

         $success['token'] = $user->createToken('test')->accessToken;
         return response()->json(['success' => $success], $this->successResponse);
    }

    public function login(){
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){
            $user = Auth::user();
            $success['token'] = $user->createToken('test')->accessToken;
            return response()->json(['success' => $success], $this->successResponse);
        }else{
            return response()->json(['error' => 'Unauthorised'], 401);
        }
    }

    public function getUser(){
        $user = Auth::user();
        return response()->json(['success' => $user], $this->successResponse);
    }
}

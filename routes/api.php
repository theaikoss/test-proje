<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/v1/register', 'Api\AuthController@register');
Route::post('/v1/login', 'Api\AuthController@login');

Route::prefix('/v1')->middleware('auth:api')->namespace('Api')->group(function(){
    Route::get('products', 'ProductController@index');
    Route::get('product/{id}', 'ProductController@show');
});


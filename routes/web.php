<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::namespace('Backend')->middleware('auth')->group(function(){
    Route::get('dashboard', 'DashboardController@index');
    Route::resource('product', 'ProductController');
    Route::resource('category', 'CategoryController');
});

Route::namespace('Frontend')->group(function(){
    Route::get('/', 'ProductController@index');
    Route::get('product/category/{id}', 'ProductController@byCategory')->name('product.category');
    Route::get('product.show/{id}', 'ProductController@show')->name('product.show');
});
// Route::get('/dashboard', 'Dashboard@index');

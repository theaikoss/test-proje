@extends('frontend.layouts.app')
@section('content')
<div class="container">
  <div class="row">
  <div class="col-md-3">
    <h3>Kategoriler</h3>
    @foreach($categories as $category)
        <li><a href="{{route('product.category', $category->id)}}">@if($category->products_count > 0){{$category->name}} ({{$category->products_count}}) @endif</a></li>
    @endforeach
        <li><a href="{{url('/')}}">Tüm Ürünler</a></li>
      </ul>
  </div>

  <div class="col-md-9">
  <div class="row">
  @foreach ($products as $product)
    <div class="col-sm-4 mb-3">
      <div class="card">
      <img class="card-img-top" src="{{url('/')}}{{$product->product_thumbnail}}" alt="{{$product->product_name}}">
        <div class="card-body">
          <h5 class="card-title">{{$product->product_name}}</h5>
          <p class="card-text">{{$product->product_content }}</p>
          <p class="card-text">{{$product->category->name}}</p>
          <a href="{{ route('product.show', $product->id) }}" class="btn btn-primary">İncele</a>
        </div>
      </div>
    </div>
  @endforeach
  </div>
  </div>
  </div>
</div>
@endsection
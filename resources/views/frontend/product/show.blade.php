@extends('frontend.layouts.app')
@section('content')
<div class="container">
  <div class="row">
  <div class="col-md-3">
    <h3>Kategoriler</h3>
      <ul>
    @foreach($categories as $category)
        <li><a href="{{route('product.category', $category->id)}}">
          @if($category->products_count > 0){{$category->name}} ({{$category->products_count}}) @endif</a>
        </li>
    @endforeach
        <li><a href="{{url('/')}}">Tüm Ürünler</a></li>
      </ul>
  </div>

  <div class="col-md-9">
    <h2>{{$product->product_name}}</h2>
    <img class="img-responsive" src="{{url('/')}}{{$product->product_thumbnail}}" alt="{{$product->product_name}}">
    <div>
    Yazar: <span>{{$product->product_content}}</span> <br>
    Stok: <span>{{$product->product_stock}}</span>
    </div>
  </div>
  </div>
</div>
@endsection
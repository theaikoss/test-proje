@extends('backend.layouts.backendapp')

@section('content')
<div class="col-9">
  <div class="content-wrapper py-3 border-bottom">
      <div class="content-head d-flex justify-content-between align-items-center mb-3">
        <h3 class="b-inline">Kategoriler</h3>
        <a href="{{ route('category.create') }}" class="d-inline btn btn-success">Yeni Ekle</a>
      </div>
      <div class="content">
        <table class="table">
          <thead class="thead-dark">
            <tr>
              <th scope="col">#</th>
              <th scope="col">Kategori Adı</th>
              <th scope="col">Ürün Sayısı</th>
              <th scope="col">İşlem</th>
            </tr>
          </thead>
          <tbody>
          @foreach ($categories as $category)
            <tr>
              <th scope="row">{{ $category->id }}</th>
              <td>{{ $category->name }}</td>
              <td>{{ $category->products_count }}</td>
              <td>
              <a href="{{ route('category.edit', $category->id) }}" class="btn btn-primary"><i class="fas fa-edit fa-lg color:green;"></i></a>
              <a href="#" 
                  onclick="var result=confirm('Silmek istediğinizden emin misiniz?');
                  if(result){
                    event.preventDefault();
                    document.getElementById('delete-form').submit();
                  }" class="btn btn-danger"><i class="fas fa-trash fa-lg"></i></a>
                <form id="delete-form" action="{{ route('category.destroy', $category->id) }}" method="POST" style="display:none;">
                  @csrf
                  <input type="hidden" name="_method" value="delete">
                </form>
              </td>
            </tr>
          @endforeach
          </tbody>
        </table>
      </div>
  </div>
</div>
@endsection
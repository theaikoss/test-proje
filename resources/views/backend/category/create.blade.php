@extends('backend.layouts.backendapp')

@section('content')
<div class="col-9">
  <div class="content-wrapper py-3 border-bottom">
      <div class="content-head d-flex justify-content-between align-items-center mb-3">
        <h3 class="b-inline">Kategoriler</h3>
        <a href="{{ route('category.index') }}" class="d-inline btn btn-success">Tüm Kategoriler</a>
      </div>
      <div class="content">
        <form method="POST" action="{{ route('category.store') }}" enctype="multipart/form-data">
          @csrf
            <div class="form-group col-md-4">
              <label for="productName">Kategori Adı</label>
              <input type="text" name="name" class="form-control">
            </div>
          <button type="submit" class="btn btn-primary">Kaydet</button>
        </form>
      </div>
  </div>
</div>
@endsection
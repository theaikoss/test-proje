@extends('backend.layouts.backendapp')

@section('content')
<div class="col-9">
  <div class="content-wrapper py-3 border-bottom">
      <div class="content-head d-flex justify-content-between align-items-center mb-3">
        <h3 class="b-inline">Ürünler</h3>
        <a href="{{ route('product.index') }}" class="d-inline btn btn-success">Tüm Ürünler</a>
      </div>
      <div class="content">
        <form method="POST" action="{{ route('product.store') }}" enctype="multipart/form-data">
          @csrf
          <div class="form-row">
            <div class="form-group col-md-4">
              <label for="productName">Ürün Adı</label>
              <input type="text" name="product_name" class="form-control">
            </div>
            <div class="form-group col-md-4">
              <label for="productCategory">Ürün Kategorisi</label>
              <select id="productCategory" name="category_id" class="form-control">
                <option selected>Seç...</option>
                @foreach ($categories as $category)
                <option value="{{$category->id}}">{{$category->name}}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group col-md-4">
              <label for="productStok">Stok</label>
              <input type="text" name="product_stock" class="form-control">
            </div>
          </div>
          <div class="form-row">
            <div class="form-group col-md-6">
              <label for="exampleFormControlTextarea1">Ürün Açıklaması</label>
              <textarea name="product_content" class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
            </div>
            <div class="form-group col-md-6">
              <label for="product_thumbnail">Ürün Resmi</label>
              <input name="product_thumbnail" type="file" class="form-control-file" id="product_thumbnail">
            </div>
          </div>
          <button type="submit" class="btn btn-primary">Kaydet</button>
        </form>
      </div>
  </div>
</div>
@endsection
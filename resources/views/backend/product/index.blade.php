@extends('backend.layouts.backendapp')

@section('content')
<div class="col-9">
  <div class="content-wrapper py-3 border-bottom">
      <div class="content-head d-flex justify-content-between align-items-center mb-3">
        <h3 class="b-inline">Ürünler</h3>
        <a href="{{ route('product.create') }}" class="d-inline btn btn-success">Yeni Ekle</a>
      </div>
      <div class="content">
        <table class="table">
          <thead class="thead-dark">
            <tr>
              <th scope="col">#</th>
              <th scope="col">Ürün Adı</th>
              <th scope="col">Ürün Kategori</th>
              <th scope="col">Stock</th>
              <th scope="col">İşlem</th>
            </tr>
          </thead>
          <tbody>
          @foreach ($products as $product)
            <tr>
              <th scope="row">{{$product->id}}</th>
              <td>{{$product->product_name}}</td>
              <td>{{$product->category->name}}</td>
              <td>{{$product->product_stock}}</td>
              <td>
                <a href="{{ route('product.edit', $product->id) }}"><i class="fas fa-edit fa-lg" style="color:green;margin-right:10px;"></i></a>
                <a href="#" 
                  onclick="var result=confirm('Silmek istediğinizden emin misiniz?');
                  if(result){
                    event.preventDefault();
                    document.getElementById('delete-form').submit();
                  }"
                  ><i class="fas fa-trash-alt fa-lg" style="color:red;"></i></a>
                <form id="delete-form" action="{{ route('product.destroy', $product->id) }}" method="POST" style="display:none;">
                  @csrf
                  <input type="hidden" name="_method" value="delete">
                </form>
              </td>
            </tr>
          @endforeach
          </tbody>
        </table>
      </div>
  </div>
</div>
@endsection